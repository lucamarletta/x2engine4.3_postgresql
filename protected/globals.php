<?php

/**
 * This is the shortcut to DIRECTORY_SEPARATOR
 */
defined('DS') or define('DS', DIRECTORY_SEPARATOR);

/**
 * This is the shortcut to Yii::app()
 */
function app() {
  return Yii::app();
}

/**
 * This is the shortcut to Yii::app()->clientScript
 */
function cs() {
  // You could also call the client script instance via Yii::app()->clientScript
  // But this is faster
  return Yii::app()->getClientScript();
}

/**
 * This is the shortcut to Yii::app()->user.
 */
function user() {
  return Yii::app()->getUser();
}

/**
 * This is the shortcut to Yii::app()->createUrl()
 */
function url($route, $params = array(), $ampersand = '&') {
  return Yii::app()->createUrl($route, $params, $ampersand);
}

/**
 * This is the shortcut to CHtml::encode
 */
function h($text) {
  return htmlspecialchars($text, ENT_QUOTES, Yii::app()->charset);
}


function eu($val) {
  return Helpers::formatEuro($val);
}

function pg_ar($pgArray, $start = 0) {
  return Helpers::phpArray($pgArray, $start);
}

/**
 * This is the shortcut to CHtml::link()
 */
function l($text, $url = '#', $htmlOptions = array()) {
  return CHtml::link($text, $url, $htmlOptions);
}

/**
 * This is the shortcut to Yii::t() with default category = 'stay'
 */
function t($message, $category = 'stay', $params = array(), $source = NULL, $language = NULL) {
  return Yii::t($category, $message, $params, $source, $language);
}

/**
 * This is the shortcut to Yii::app()->request->baseUrl
 * If the parameter is given, it will be returned and prefixed with the app baseUrl.
 */
function bu($url = NULL) {
  static $baseUrl;
  if ($baseUrl === NULL)
    $baseUrl = Yii::app()->getRequest()->getBaseUrl();
  return $url === NULL ? $baseUrl : $baseUrl . '/' . ltrim($url, '/');
}

/**
 * Returns the named application parameter.
 * This is the shortcut to Yii::app()->params[$name].
 */
function param($name) {
  return Yii::app()->params[$name];
}


function dump($target) {
  return CVarDumper::dump($target, 10, true);
}